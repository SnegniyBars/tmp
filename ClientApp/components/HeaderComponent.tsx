import * as React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { ApplicationState } from '../store';
import * as AuthorizationStore from "../store/Authorization";

let store = connect()


type HeaderComponentProps =
    AuthorizationStore.AuthorizationState
    & RouteComponentProps<{}>;

export default class HeaderComponent extends React.Component<{}, {}> {
    constructor(props:any){
        super(props);

        
    }
    
    componentDidMount() {        
    }

    specialValueSelected(options: any, specialValue: any) {        
    }

    noneOfTheAboveHandler(survey: any, options: any) {        
    }

    componentWillUnmount() {
    }
    componentWillMount() {

	}

    public render() {        
        //const { authorize } = this.props;
        
        var userName = '';
        var userId = '';
        var isUserAuthorized = false;
        // switch (authorize.type) {
        //     case 'NONE_AUTHORIZED_USER':                
        //         isUserAuthorized = false;                
        //         break;
        //     case 'AUTHORIZED_USER':
        //         isUserAuthorized = false;
        //         var a = authorize as AuthorizationStore.AuthorizedUser;                
        //         userName = a.userName;
        //         userId = a.userId;                    
        //         break;
        //     default:
        //         isUserAuthorized = false;
        // }

        
        return <div className="main-navbar2">
            <div className="navbar-sectionLeft">
                <a href="/" className="logo">
                    {/* add image here */}
                </a>
                <ul className="nav-links" id="navbar-links">
                    <li className="nav-links__item">
                        <a href="/" className="nav-links__item-link nav-links__item-link_current">Публикации</a>
                    </li>  
                </ul>
            </div>
            <div className="navbar-sectionRight">                
                {isUserAuthorized && <a href='/'>{userName}</a>}
                {!isUserAuthorized && <div><a href='/login'>Войти</a>/<a href='/signup'>Зарегистрироваться</a></div>}
            </div>
        </div>;
    }
}


// // Wire up the React component to the Redux store
// export default connect(
//     (state: ApplicationState) => state.authorization, // Selects which state properties are merged into the component's props
//     AuthorizationStore.actionLogin                 // Selects which action creators are merged into the component's props
// )(HeaderComponent) as typeof HeaderComponent;