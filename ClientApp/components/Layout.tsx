import * as React from 'react';
import { NavMenu } from './NavMenu';
import LoginForm from '../components/LoginForm';
import HeaderComponent from '../components/HeaderComponent';


export class Layout extends React.Component<{}, {}> {
    public render() {
        return <div className='container-fluid'>                        
            <div><HeaderComponent/></div>
            <div className='row'>
                 {/* <div className='col-sm-3'>
                     <NavMenu />
                </div> */}
                <div className='col-sm-9'>
                    { this.props.children }
                </div>
            </div>
        </div>;
    }
}
