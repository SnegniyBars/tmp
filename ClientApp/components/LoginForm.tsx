﻿import * as React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { ApplicationState } from '../store';
import * as AuthorizationStore from "../store/Authorization";


type LoginFormProps =
    AuthorizationStore.AuthorizationState
    & typeof AuthorizationStore.actionLogin
    & RouteComponentProps<{}>;

interface State {    
    username: string;
    password: string;
    submitted: boolean;
}

class LoginForm extends React.Component<LoginFormProps, State> {

    public state: State = {
        username: '',
        password: '',
        submitted: false
    };

    constructor(props: LoginFormProps) {
        super(props);
        
        this.state = {
            username: '',
            password: '',
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.props.logout();
    }

    handleSubmit(e: any) {
        e.preventDefault();
        const { username, password, submitted } = this.state;
        this.setState({ submitted: true });        
        this.props.login(username, password);
    }

    handleChange(e: any) {
        e.preventDefault();
        const { name, value } = e.target;
        this.setState({ [name]: value });
        this.props.clearError();
    }

    
    componentDidMount() {        
    }

    specialValueSelected(options: any, specialValue: any) {        
    }

    noneOfTheAboveHandler(survey: any, options: any) {        
    }

    componentWillUnmount() {
    }

    

    //<form onSubmit={() => this.onSubmit}>
    //</form>
    public render() {
        

        const { username, password, submitted } = this.state;
        const { authorize } = this.props;
        var isNonAuthUser = false;
        var message = '';

        switch (authorize.type) {
            case 'NONE_AUTHORIZED_USER':
                var na = authorize as AuthorizationStore.NoneAuthorizedUser;
                isNonAuthUser = true;
                message = na.message;
                break;
            case 'AUTHORIZED_USER':
                var a = authorize as AuthorizationStore.AuthorizedUser;
                isNonAuthUser = false;
                var userName = a.userName;
                var userId = a.userId;                    
                this.props.history.push("/")
                break;
            default:
                isNonAuthUser = true;
        }

        //var username = '';
        //<input type="text" className="form-control" name="username" value={username} onChange={this.handleChange} />
        //{ this.props.count > 1 && this.props.count < 3 && <div className="help-block">Username is required</div> }
        return <div>            
            <h2>Login</h2>
            <form name="form" onSubmit={this.handleSubmit}>
                
                <label htmlFor="username">Username</label>
                <input type="text" className="form-control" name="username" value={username} onChange={this.handleChange} />

                <label htmlFor="password">Password</label>
                <input type="password" className="form-control" name="password" value={password} onChange={this.handleChange} />

                {message && isNonAuthUser && <div className="has-error"><div className="help-block">{message}</div></div>}
                {!isNonAuthUser && <div className="help-block">{(authorize as AuthorizationStore.AuthorizedUser).userName}</div>}
                <button className="btn btn-primary">Login</button>                
            </form>

            <h1>Counter</h1>


            <p>This is a simple example of a React component.</p>

            <p>Current count: <strong>{this.props.count}</strong></p>
            <input value={this.props.count} />

            <button onClick={() => { this.props.login("111","222") }}>Increment</button>
            <button onClick={() => { this.props.logout() }}>Decrement</button>
        </div>;
        
    }
}


// Wire up the React component to the Redux store
export default connect(
    (state: ApplicationState) => state.authorization, // Selects which state properties are merged into the component's props
    AuthorizationStore.actionLogin                 // Selects which action creators are merged into the component's props
)(LoginForm) as typeof LoginForm;