﻿import { Action, Reducer } from 'redux';

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface NoneAuthorizedUser {
    type: 'NONE_AUTHORIZED_USER',
    message: string,
}
export interface AuthorizedUser {
    type: 'AUTHORIZED_USER',
    userName: string,
    userId: string,
}


export type AuthorizedUserState = NoneAuthorizedUser | AuthorizedUser;
// export type AuthorizedUserState =
//     {
//         type: 'NONE_AUTHORIZED_USER',
//         message: string,
//     }
//     |
//     {
//         type: 'AUTHORIZED_USER',
//         userName: string,
//         userId: string,
//     };

export interface AuthorizationState {
    count: number;
    authorize: AuthorizedUserState;
}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.
// Use @typeName and isActionType for type detection that works even after serialization/deserialization.

interface LoginAction { type: 'LOGIN', login: string, password: string }
interface LogoutAction { type: 'LOGOUT' }
interface RegisterAction { type: 'REGISTER' }
interface ClearErrorAction { type: 'CLEARERRORACTION' }

// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
type KnownAction = LoginAction | LogoutAction | ClearErrorAction;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionLogin = {
    //login: () => <LoginAction>{ type: 'LOGIN' },
    login: (username: string, password: string) => <LoginAction>{ type: 'LOGIN', login: username, password: password },
    logout: () => <LogoutAction>{ type: 'LOGOUT' },
    clearError: () => <ClearErrorAction>{ type: 'CLEARERRORACTION' }
};

export const actionRegister = {
    register: () => <RegisterAction>{ type: 'REGISTER' }
};

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

const noneAuthorizedUser: NoneAuthorizedUser = { type: 'NONE_AUTHORIZED_USER', message: '' };

// function handleResponse(response:any) {
//     return new Promise((resolve, reject) => {
//         if (response.ok) {
//             // return json if it was returned in the response
//             var contentType = response.headers.get("content-type");
//             if (contentType && contentType.includes("application/json")) {
//                 response.json().then(json => resolve(json));
//             } else {
//                 resolve();
//             }
//         } else {
//             // return error message from response body
//             response.text().then(text => reject(text));
//         }
//     });
// }
 
// function handleError(error:any) {
//     return Promise.reject(error && error.message);
// }

export const reducer: Reducer<AuthorizationState> = (state: AuthorizationState, action: KnownAction) => {

    //alert(JSON.stringify(state));
    switch (action.type) {
        case 'LOGIN':
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ login:action.login, password:action.password })
            };
     
            // var user = fetch('/users/authenticate', requestOptions)
            //         .then(handleResponse, handleError)
            //         .then(user => {
            //             // login successful if there's a jwt token in the response
            //             if (user && user.token) {
            //                 // store user details and jwt token in local storage to keep user logged in between page refreshes
            //                 localStorage.setItem('user', JSON.stringify(user));
            //             }
            //  
            //             return user;
            //         });

            if (action.login == '111') {
                return {
                    count: state.count + 1,
                    authorize: {
                        type: 'AUTHORIZED_USER',
                        userName: '111',
                        userId: '222222',
                    }
                };
            } else {
                return {
                    count: state.count + 1,
                    authorize: { type: 'NONE_AUTHORIZED_USER', message: 'wrongUser' }
                };
            };
        case 'LOGOUT':
            if (state.count <= 0)
                return { count: 0, authorize: noneAuthorizedUser };
            else
                return { count: state.count - 1, authorize: noneAuthorizedUser };
        case 'CLEARERRORACTION':
             return {
                count: 0,
                authorize: { type: 'NONE_AUTHORIZED_USER', message: '' }
            };
        default:
            // The following line guarantees that every action in the KnownAction union has been covered by a case above
            const exhaustiveCheck: never = action;
    }

    // For unrecognized actions (or in cases where actions have no effect), must return the existing state
    //  (or default initial state if none was supplied)
    return state || { count: 0, authorize: noneAuthorizedUser };
};



export const userConstants = {
    REGISTER_REQUEST: 'USERS_REGISTER_REQUEST',
    REGISTER_SUCCESS: 'USERS_REGISTER_SUCCESS',
    REGISTER_FAILURE: 'USERS_REGISTER_FAILURE',

    LOGIN_REQUEST: 'USERS_LOGIN_REQUEST',
    LOGIN_SUCCESS: 'USERS_LOGIN_SUCCESS',
    LOGIN_FAILURE: 'USERS_LOGIN_FAILURE',

    LOGOUT: 'USERS_LOGOUT',

    GETALL_REQUEST: 'USERS_GETALL_REQUEST',
    GETALL_SUCCESS: 'USERS_GETALL_SUCCESS',
    GETALL_FAILURE: 'USERS_GETALL_FAILURE',

    DELETE_REQUEST: 'USERS_DELETE_REQUEST',
    DELETE_SUCCESS: 'USERS_DELETE_SUCCESS',
    DELETE_FAILURE: 'USERS_DELETE_FAILURE'
};


// export function registerUser({ email, firstName, lastName, password }) {  
//     return function(dispatch) {
//       axios.post(`${API_URL}/auth/register`, { email, firstName, lastName, password })
//       .then(response => {
//         cookie.save('token', response.data.token, { path: '/' });
//         dispatch({ type: AUTH_USER });
//         window.location.href = CLIENT_ROOT_URL + '/dashboard';
//       })
//       .catch((error) => {
//         errorHandler(dispatch, error.response, AUTH_ERROR)
//       });
//     }
//   }